# Команды для изначального сценария --------------------------------

# Компиляция сервера ++++
build:
	go build -o server main
	
# Запуск сервера ++++
run:
	./server

# Удаление сервера
del: 
	rm -f ./server

# Автоматизированная установка и запуск ----------------------------


# Запуск и компиляция сервера
server: build run

# Сервисные команды (на будущее) -----------------------------------

# Запускаем линтер (его конфиг - .golangci.yml)
lint:
	golangci-lint run --fix

# Проверка кода на наличие ошибок и неправильных конструкций. 
# "./..." - все файлы в текущей директории и поддиректориях. 
vet:
	go vet ./...

# Создание go.mod
mod:
	go mod init main

# Обновление файла go.mod и удаление неиспользуемых зависимостей 
tidy:
	go mod tidy

# Загрузка модулей в кеш 
down:
	go mod download

# Очистка
clean: clean_cache del_server 

# Docker-образы
ims:
	docker images

# Запущенные Docker-контейнеры
ps:
	docker ps

# Служебные части --------------------------------------------------

# Очистка кеша
clean_cache:
	go clean

# Создать сервер через Dockerfile
docker_build:
	docker build -f docker/Dockerfile -t server_cnt .
#	docker build --no-cache -f docker/Dockerfile -t server_cnt .

# Запустить сервер в Docker
docker_run:
	docker run -p 5000:5000 --rm --name server_container server_cnt

# Остановить сервер в Docker 
docker_stop:
	docker stop $$(docker ps --filter "ancestor=server_cnt" -q)

# docker stop $$(docker ps | grep server_cnt | awk '{print $1}')


# Команды для Docker-Compose ---------------------------------------

# Запуск docker-compose для server
compose_srv:
	docker compose -f docker-compose-srv.yaml up --remove-orphans

# Остановить docker-compose для server
compose_stop_srv:
	docker compose -f docker-compose-srv.yaml down --remove-orphans

# Пересобрать docker-compose для server
compose_rebuild_srv:
	docker compose -f docker-compose-srv.yaml build 









