package main

import (
	"fmt"
	"log"
	"net/http"
)

func main() {

	// API routes (тест после обновления)
	http.Handle(
		"/",
		http.FileServer(http.Dir("./static")),
	)
	http.HandleFunc("/hi", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "Сервер в рабочем состоянии!!")
	})

	port := ":5000"
	fmt.Println("Server is running on port" + port)
	fmt.Println("Для проверки работы сервера перейдите по ссылке: http://localhost:5000/hi")
	fmt.Println("Для проверки обслуживания сервером статических файлов перейдите по ссылкам")
	fmt.Println("1: http://localhost:5000/")
	fmt.Println("2: http://localhost:5000/about.html")
	fmt.Println("3: http://localhost:5000/GFG.png")

	// Start server on port specified above
	log.Fatal(http.ListenAndServe(port, nil))

}
